
<?php
error_reporting(0);
set_time_limit (0);



echo "<head><link rel='stylesheet' type='text/css' href='bootstrap.min.css' />";   
 echo "<link rel='stylesheet' type='text/css' href='style.css' /></head><body>";  


?>


    <div class="container">
        <div class="card card-container">

			<img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" /><h1>Purchasing Agent Finder</h1></h1>
            <p id="profile-name" class="profile-name-card"></p>
            <form class="form-signin" action="FindContacts.php" method="get">
                <span id="reauth-email" class="reauth-email"></span>
                <input type="text" name="company" id="company" class="form-control" placeholder="Company Name" required autofocus><br>
		<select class="form-control" id="state" name="state">
			<option value="All">All States</option>
			<option value="Alaska">Alaska</option>
			<option value="Alabama">Alabama</option>
			<option value="Arkansas">Arkansas</option>
			<option value="Arizona">Arizona</option>
			<option value="California">California</option>
			<option value="Colorado">Colorado</option>
			<option value="Connecticut">Connecticut</option>
			<option value="Washington, DC">Washington, DC</option>
			<option value="Delaware">Delaware</option>
			<option value="Florida">Florida</option>
			<option value="Georgia">Georgia</option>
			<option value="Hawaii">Hawaii</option>
			<option value="Iowa">Iowa</option>
			<option value="Idaho">Idaho</option>
			<option value="Illinois">Illinois</option>
			<option value="Indiana">Indiana</option>
			<option value="Kansas">Kansas</option>
			<option value="Kentucky">Kentucky</option>
			<option value="Louisiana">Louisiana</option>
			<option value="Massachusetts">Massachusetts</option>
			<option value="Maryland">Maryland</option>
			<option value="Maine">Maine</option>
			<option value="Michigan">Michigan</option>
			<option value="Minnesota">Minnesota</option>
			<option value="Missouri">Missouri</option>
			<option value="Mississippi">Mississippi</option>
			<option value="Montana">Montana</option>
			<option value="North Carolina">North Carolina</option>
			<option value="North Dakota">North Dakota</option>
			<option value="Nebraska">Nebraska</option>
			<option value="New Hampshire">New Hampshire</option>
			<option value="New Jersey">New Jersey</option>
			<option value="New Mexico">New Mexico</option>
			<option value="Nevada">Nevada</option>
			<option value="New York">New York</option>
			<option value="Ohio">Ohio</option>
			<option value="Oklahoma">Oklahoma</option>
			<option value="Oregon">Oregon</option>
			<option value="Pennsylvania">Pennsylvania</option>
			<option value="Puerto Rico">Puerto Rico</option>
			<option value="Rhode Island">Rhode Island</option>
			<option value="South Carolina">South Carolina</option>
			<option value="South Dakota">South Dakota</option>
			<option value="Tennessee">Tennessee</option>
			<option value="Texas">Texas</option>
			<option value="Utah">Utah</option>
			<option value="Virginia">Virginia</option>
			<option value="Vermont">Vermont</option>
			<option value="Washington">Washington</option>
			<option value="Wisconsin">Wisconsin</option>
			<option value="West Virginia">West Virginia</option>
			<option value="Wyoming">Wyoming</option>
		</select>
<br>

				<input type="text" name="url" id="url" class="form-control" placeholder="Website (Optional)"><br>
                <br><small>*Website is used to guess email addresses.</small>
                <br><small>*State is used to filter results by state. </small>
<br><br>
                <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Search LinkedIn</button>
            </form><!-- /form -->
           
        </div><!-- /card-container -->
    </div><!-- /container -->
    </body>